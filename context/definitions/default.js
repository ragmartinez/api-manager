module.exports = {
  name: process.env.NODE_CONTEXT_NAME || 'fif-payments-api-manager-points',
  port: process.env.NODE_CONTEXT_PORT || 2712,
  middlewares: {
    getPoints: process.env.NODE_CONTEXT_MIDDLEWARES_GET_POINTS || [
      'check-country-middleware',
      'validate-jwt-middleware',
      'get-points-middleware',
    ]
  },
  country: process.env.NODE_CONTEXT_COUNTRY || 'cl',
  version: process.env.NODE_CONTEXT_VERSION || 'v1',
  channel: process.env.NODE_CONTEXT_CHANNEL || 'web',
  blacklistData: process.env.NODE_CONTEXT_BLACKLIST_DATA || ''
};
