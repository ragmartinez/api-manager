const context = require('./app/config/').context;
const configLog = require('./app/log-config/config').init;
const config = require('./app/config/').config;

if (config.enabled_instana) {
  // eslint-disable-next-line global-require
  require('@instana/collector')();
}
const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
const YAML = require('yamljs');
const swaggerUi = require('swagger-ui-express');


const { asyncHookCreate,
  traceMiddleware,
  getTraceHeaders } = require('fif-common-node-logger');
const distributedTracing = require('distributed-tracing');

distributedTracing(getTraceHeaders);

const asyncHook = asyncHookCreate(configLog.nameSpaceHook);

/** up server */
const app = express();
const APIRoutes = require('./app/routes');

const swaggerDocument = YAML.load(`./swagger-${context.version}.yml`);
const BASE_URL = '';


app.set('trust proxy', true);
app.use(traceMiddleware(asyncHook));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(compression());

/** health check MS */
app.get(`${BASE_URL}/${context.version}/health`, (req, res) => {
  res.status(200).send({ code: 'OK', message: `${context.name} up and running` });
});

/** docs */
app.use(
  `${BASE_URL}/${context.version}/api-docs`,
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocument)
);

/** endpoints */

app.use(BASE_URL, APIRoutes);

app.use((err, res) => {
  console.log(err.message);
  res.status(500).send({ code: 'error', message: 'internal error not handled' });
});

module.exports = app;
