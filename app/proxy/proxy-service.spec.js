const mockery = require('mockery');
const chai = require('chai');
const expect = chai.expect;
const sandbox = require('sinon').createSandbox();
const axios = require('axios');

describe('Proxy', () => {

  afterEach(function () {
    sandbox.restore();
    mockery.disable();
    mockery.deregisterAll();
  });

  beforeEach(function () {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
    });
    mockery.registerMock('../config', {
      config: {
        timeoutConnection: 400
      }
    });
    let logger = {
      info: () => {},
      debug: () => {},
      warn: () => {},
      error: () => {},
      fatal: () => {}
    }
    let mockLogger = {
      Logger: { getLogger: () => logger }
    }
    mockery.registerMock('fif-common-node-logger', mockLogger);
  });

  describe("method", () => {

    it("should return an ok", async () => {
      const okResponse = new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          statusText: "OK",
          headers: {
            'content-type': 'application/json'
          },
          data: {
            ok: true
          }
        });
      });

      sandbox
        .stub(axios, 'request')
        .returns(okResponse);

      path = '/dummy-path'
      const proxy = require('./proxy-service');
      const response = await proxy(path);
      expect(response.ok).to.be.true;
    });

    it("should return an error of connection", async () => {
      const response = new Promise((resolve, reject) => {
        return reject({
          status: 500,
          stack:  "Dummy code",
          message: 'Dummy message',
          headers: {
            'content-type': 'application/json'
          },
        });
      });

      sandbox
        .stub(axios, 'request')
        .returns(response);

      path = '/dummy-path'
      const proxy = require('./proxy-service');
      try {
        await proxy(path);
      } catch (error) {
        expect(error).to.be.not.null;
        expect(error.status).to.be.eq(500);
        expect(error.code).to.be.eq("ERROR_OF_CONNECTION");
      }
    });

    it("should return an error of timeout", async () => {
      const response = new Promise((resolve, reject) => {
        return reject({
          status: 500,
          stack:  "Error: timeout",
          message: 'Dummy message',
          headers: {
            'content-type': 'application/json'
          },
        });
      });

      sandbox
        .stub(axios, 'request')
        .returns(response);

      path = '/dummy-path'
      const proxy = require('./proxy-service');
      try {
        await proxy(path, 'get', 'body', null);
      } catch (error) {
        expect(error).to.be.not.null;
        expect(error.status).to.be.eq(408);
        expect(error.code).to.be.eq("ERROR_OF_TIMEOUT");
      }
    });

    it("should return an error when the struct is error_code", async () => {
      const okResponse = new Promise((resolve, reject) => {
        return reject({
          response:{
            status: 400,
            data: {
              error_code: 'DUMMY_CODE',
              error_description: 'DUMMY_MESSAGE'
            }
          },
          headers: {
            'content-type': 'application/json'
          },
        });
      });

      sandbox
        .stub(axios, 'request')
        .returns(okResponse);

      path = '/dummy-path'
      const proxy = require('./proxy-service');
      try {
        await proxy(path);
      } catch (error) {
        expect(error).to.be.not.null;
        expect(error.status).to.be.eq(400);
        expect(error.name).to.be.eq("BadRequestError");
        expect(error.code).to.be.eq("DUMMY_CODE");
      }
    });

    it("should return an error when the struct is status", async () => {
      const okResponse = new Promise((resolve, reject) => {
        return reject({
          response:{
            status: 400,
            data: {
              status: 'DUMMY_CODE',
              error_description: 'DUMMY_MESSAGE'
            }
          },
          headers: {
            'content-type': 'application/json'
          },
        });
      });

      sandbox
        .stub(axios, 'request')
        .returns(okResponse);

      path = '/dummy-path'
      const proxy = require('./proxy-service');
      try {
        await proxy(path);
      } catch (error) {
        expect(error).to.be.not.null;
        expect(error.status).to.be.eq(400);
        expect(error.name).to.be.eq("BadRequestError");
        expect(error.code).to.be.eq("DUMMY_CODE");
      }
    });

    it("should return an error when the struct is not defind", async () => {
      const okResponse = new Promise((resolve, reject) => {
        return reject({
          response:{
            status: 400,
            data: {
              data: 'DUMMY_CODE',
              error_description: 'DUMMY_MESSAGE'
            }
          },
          headers: {
            'content-type': 'application/json'
          },
        });
      });

      sandbox
        .stub(axios, 'request')
        .returns(okResponse);

      path = '/dummy-path'
      const proxy = require('./proxy-service');
      try {
        await proxy(path);
      } catch (error) {
        expect(error).to.be.not.null;
        expect(error.status).to.be.eq(400);
        expect(error.name).to.be.eq("BadRequestError");
        expect(error.code).to.be.eq("NOT_CODE");
      }
    });

    it("should throw an error when is a error and de body is string", async () => {
      const okResponse = new Promise((resolve, reject) => {
        return reject({
          response:{
            status: 400,
            data: JSON.stringify({
              data: 'DUMMY_CODE',
              error_description: 'DUMMY_MESSAGE'
            })
          },
        });
      });
      mockery.registerMock('../../config', {
        config: {
          timeoutConnection: '400'
        }
      });
      sandbox
        .stub(axios, 'request')
        .returns(okResponse);

      path = '/dummy-path'
      const proxy = require('./proxy-service');
      try {
        await proxy(path);
      } catch (error) {
        expect(error).to.be.not.null;
        expect(error.status).to.be.eq(400);
        expect(error.name).to.be.eq("BadRequestError");
        expect(error.code).to.be.eq("NOT_CODE");
      }
    });

    it("should return an ok when is body is string", async () => {
      const okResponse = new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          data: JSON.stringify({
            data: 'DUMMY_CODE',
            error_description: 'DUMMY_MESSAGE'
          })
        });
      });
      mockery.registerMock('../config', {
        config: {
          timeoutConnection: '400'
        }
      });
      sandbox
        .stub(axios, 'request')
        .returns(okResponse);

      path = '/dummy-path'
      const proxy = require('./proxy-service');
      const response = await proxy(path);
      expect(response).to.be.not.null;
      expect(response.data).to.be.eq("DUMMY_CODE");
    });

    describe("should throw an error if status is", () => {
      it("400", async () => {
        const okResponse = new Promise((resolve, reject) => {
          return reject({
            response:{
              status: 400,
              data: {
                code: 'DUMMY_CODE',
                message: 'DUMMY_MESSAGE'
              }
            },
            headers: {
              'content-type': 'application/json'
            },
          });
        });

        sandbox
          .stub(axios, 'request')
          .returns(okResponse);

        path = '/dummy-path'
        const proxy = require('./proxy-service');
        try {
          await proxy(path);
        } catch (error) {
          expect(error).to.be.not.null;
          expect(error.status).to.be.eq(400);
          expect(error.name).to.be.eq("BadRequestError");
          expect(error.code).to.be.eq("DUMMY_CODE");
        }
      });

      it("401", async () => {
        const okResponse = new Promise((resolve, reject) => {
          return reject({
            response:{
              status: 401,
              data: {
                code: 'DUMMY_CODE',
                message: 'DUMMY_MESSAGE'
              }
            },
            headers: {
              'content-type': 'application/json'
            },
          });
        });

        sandbox
          .stub(axios, 'request')
          .returns(okResponse);

        path = '/dummy-path'
        const proxy = require('./proxy-service');
        try {
          await proxy(path);
        } catch (error) {
          expect(error).to.be.not.null;
          expect(error.status).to.be.eq(401);
          expect(error.name).to.be.eq("UnauthorizedError");
          expect(error.code).to.be.eq("DUMMY_CODE");
        }
      });

      it("403", async () => {
        const okResponse = new Promise((resolve, reject) => {
          return reject({
            response:{
              status: 403,
              data: {
                code: 'DUMMY_CODE',
                message: 'DUMMY_MESSAGE'
              }
            },
            headers: {
              'content-type': 'application/json'
            },
          });
        });

        sandbox
          .stub(axios, 'request')
          .returns(okResponse);

        path = '/dummy-path'
        const proxy = require('./proxy-service');
        try {
          await proxy(path);
        } catch (error) {
          expect(error).to.be.not.null;
          expect(error.status).to.be.eq(403);
          expect(error.name).to.be.eq("ForbiddenError");
          expect(error.code).to.be.eq("DUMMY_CODE");
        }
      });

      it("404", async () => {
        const okResponse = new Promise((resolve, reject) => {
          return reject({
            response:{
              status: 404,
              data: {
                code: 'DUMMY_CODE',
                message: 'DUMMY_MESSAGE'
              }
            },
            headers: {
              'content-type': 'application/json'
            },
          });
        });

        sandbox
          .stub(axios, 'request')
          .returns(okResponse);

        path = '/dummy-path'
        const proxy = require('./proxy-service');
        try {
          await proxy(path);
        } catch (error) {
          expect(error).to.be.not.null;
          expect(error.status).to.be.eq(404);
          expect(error.name).to.be.eq("NotFoundError");
          expect(error.code).to.be.eq("DUMMY_CODE");
        }
      });

      it("422", async () => {
        const okResponse = new Promise((resolve, reject) => {
          return reject({
            response:{
              status: 422,
              data: {
                code: 'DUMMY_CODE',
                message: 'DUMMY_MESSAGE'
              }
            },
            headers: {
              'content-type': 'application/json'
            },
          });
        });

        sandbox
          .stub(axios, 'request')
          .returns(okResponse);

        path = '/dummy-path'
        const proxy = require('./proxy-service');
        try {
          await proxy(path);
        } catch (error) {
          expect(error).to.be.not.null;
          expect(error.status).to.be.eq(422);
          expect(error.name).to.be.eq("UnprocessableEntityError");
          expect(error.code).to.be.eq("DUMMY_CODE");
        }
      });

      it("500", async () => {
        const okResponse = new Promise((resolve, reject) => {
          return reject({
            response:{
              status: 500,
              data: {
                code: 'ERROR_OF_CONNECTION',
                message: 'DUMMY_MESSAGE'
              }
            },
            headers: {
              'content-type': 'application/json'
            },
          });
        });

        sandbox
          .stub(axios, 'request')
          .returns(okResponse);

        path = '/dummy-path'
        const proxy = require('./proxy-service');
        try {
          await proxy(path);
        } catch (error) {
          expect(error).to.be.not.null;
          expect(error.status).to.be.eq(500);
          expect(error.name).to.be.eq("AppError");
          expect(error.code).to.be.eq("ERROR_OF_CONNECTION");
        }
      });
    });
  });
});
