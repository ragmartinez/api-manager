const axios = require('axios');
const config = require('../config').config;
const logger = require('fif-common-node-logger').Logger.getLogger();
const _ = require('lodash');

const {
  BadRequestError,
  UnauthorizedError,
  ForbiddenError,
  NotFoundError,
  TimeoutError,
  ConnectionError,
  AppError,
  UnprocessableEntityError,
} = require('../util/common-errors');

const moduleName = 'Proxy';

function processError(body) {
  const error = (typeof body === 'string') ? JSON.parse(body) : body;
  if (error.code) {
    return error;
  }
  if (error.error_code) {
    return {
      code: error.error_code,
      message: error.error_description
    };
  }
  if (error.status) {
    return {
      code: error.status,
      message: error.error
    };
  }
  return {
    code: 'NOT_CODE',
    message: JSON.stringify(body)
  };
}
/**
 * Service to connecting with customs api rest
 *
 * @param {String} url Url of service api rest
 * @param {String} methodToSend Method of connection(get, post, put, delete)
 * @param {*} bodyToSend
 * @param {*} headersToSend
 */
module.exports = async (url, methodToSend = 'get', bodyToSend, headersToSend) => {
  /* istanbul ignore else */
  if (!headersToSend) {
    // eslint-disable-next-line no-param-reassign
    headersToSend = {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
    };
  }
  let responseApi = {};
  const trackId = _.get(headersToSend, 'log-track-id');
  const requestObject = {
    method: methodToSend,
    headers: headersToSend,
    url,
    timeout: parseInt(config.timeoutConnection, 10),
  };
  if (bodyToSend) {
    requestObject.data = bodyToSend;
  }

  logger.info(trackId, `[${moduleName}] Send data ${JSON.stringify(requestObject)}`);

  await axios.request(requestObject).then((response) => {
    logger.info(trackId, `[${moduleName}] 'Response status ${JSON.stringify(response.status)}`);
    responseApi = (typeof response.data === 'string') ? JSON.parse(response.data) : response.data;
  }).catch((error) => {
    logger.info(trackId, `[${moduleName}] Response data error ${JSON.stringify(error)}`);
    if (error.response && error.response.status >= 300) {
      const errorData = processError(error.response.data);
      switch (error.response.status) {

        case 400:
          throw new BadRequestError(errorData.code, errorData.message);
        case 401:
          throw new UnauthorizedError(errorData.code, errorData.message);
        case 403:
          throw new ForbiddenError(errorData.code, errorData.message);
        case 404:
          throw new NotFoundError(errorData.code, errorData.message);
        case 422:
          throw new UnprocessableEntityError(errorData.code, errorData.message);
        default:
          throw new AppError(errorData.code, errorData.message);
      }
    }
    if (error && error.stack && error.stack.includes('Error: timeout')) {
      throw new TimeoutError('ERROR_OF_TIMEOUT', error.message);
    }
    throw new ConnectionError('ERROR_OF_CONNECTION', error.message);
  });
  return responseApi;
};

