const path = require('path');

module.exports = {
  enabled_instana: false,
  black_list_mock: process.env.NODE_ENV_BLACK_LIST_MOCK,
  certJwt: process.env.API_MANAGER_JWT_CERT_FILE || path.resolve(__dirname, './certs/api-manager-jwt.crt'),
  apiCmrPuntos: process.env.API_CMR_PUNTOS || 'https://api-qa.cmrpuntos.cl/loyalty/cl/v2/accounts/{accountId}/points',
  cmrPuntosApiKey: process.env.CMR_PUNTOS_API_KEY || 'AIzaSyALjpPTC76UABLe_3RrWCrd6b-f0y40p8k',
  country: process.env.NODE_CONTEXT_COUNTRY || 'cl',
};
