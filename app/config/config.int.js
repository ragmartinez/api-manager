module.exports = {
  enabled_instana: false,
  black_list_mock: process.env.NODE_ENV_BLACK_LIST_MOCK,
  apiCmrPuntos: process.env.API_CMR_PUNTOS,
  cmrPuntosApiKey: process.env.CMR_PUNTOS_API_KEY,
  country: process.env.NODE_CONTEXT_COUNTRY || 'cl',
  certJwt: process.env.API_MANAGER_JWT_CERT_FILE
};
