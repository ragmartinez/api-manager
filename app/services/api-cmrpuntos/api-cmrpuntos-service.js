const logger = require('fif-common-node-logger').Logger.getLogger();
const config = require('../../config').config;
const proxy = require('../../proxy/proxy-service');

const moduleName = 'cmrPuntosService';

const cmrPuntosService = {
  getPoints: async (accountId) => {
    const url = config.apiCmrPuntos.replace('{accountId}', accountId);

    const headers = {
      'x-country': config.country,
      'x-channel': 'Tienda',
      'x-commerce': 'CMR',
      'x-api-key': config.cmrPuntosApiKey,
    };
    try {
      logger.info(`[${moduleName}] service request to ${url}`);
      const infoResponse = await proxy(url, 'GET', null, headers);
      logger.info(`[${moduleName}] response ok.`);
      return infoResponse;
    } catch (error) {
      logger.error(`[${moduleName}] error: ${JSON.stringify(error)}`);
      throw error;
    }
  }
};

module.exports = cmrPuntosService;
