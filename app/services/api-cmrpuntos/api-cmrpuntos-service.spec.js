const mockery = require('mockery');
const chai = require('chai');
const expect = chai.expect;

describe('apiCmrPuntosService', () => {

  beforeEach(function () {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    let mockLogger = {
      info: () => { },
      debug: () => { },
      warn: () => { },
      error: () => { },
      fatal: () => { },
      obfuscate: () => { },
    }
    mockery.registerMock('fif-payments-common-logger', mockLogger);
  });

  afterEach(function () {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('execute method', () => {

    it('should return success response for apiCmrPuntos', async function () {

      mockery.registerMock('../../proxy/proxy-service', async function () {
        return {
          "code": 200,
          "status": "SUCCESS",
          "points": {
              "aboutToExpire": 0,
              "closestExpirationDate": "01/04/2021",
              "earned": 0,
              "balance": 2335986
          }
      }
      });

      const service = require('./api-cmrpuntos-service');

      let accountId  = '01174628939'

      const response = await service.getPoints(accountId);
      expect(response);
    });

    it('should throw an error when something fails in findByToken', async function () {
     

      mockery.registerMock('../../proxy/proxy-service', async function () {
        throw new Error('internal');
      });

      const service = require('./api-cmrpuntos-service');

      let accountId  = '01174628939'
      try {
        await service.getPoints(accountId);
      } catch (e) {
        expect(e.message).to.be.equal("internal");
      }
    });

  });
});
