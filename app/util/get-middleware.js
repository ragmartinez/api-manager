const camelCase = require('lodash/camelCase');

function getMiddlewares(middlewareNames) {
  const arrayOfMiddleware = [];
  try {
    middlewareNames.forEach((name) => {
      if (name.includes('(ext)')) {
        const externalModuleName = name.replace('(ext)', '');
        const middleware = require(externalModuleName); // eslint-disable-line
        arrayOfMiddleware.push(middleware);
      } else {
        const middleware = require(`./../middlewares/${name}`)[camelCase(name)]; // eslint-disable-line
        arrayOfMiddleware.push(middleware);
      }
    });
    return arrayOfMiddleware;
  } catch (e) {
    throw e;
  }
}

module.exports.getMiddlewares = getMiddlewares;
