const logger = require('fif-common-node-logger').Logger.getLogger();
const jwt = require('jsonwebtoken');
const fs = require('fs');
const UnauthorizedError = require('../util/common-errors').UnauthorizedError;
const config = require('../config').config;

const schemesSupported = /^Bearer|ApiKey$/i;


function getToken(req) {
  if (req.headers && req.headers.authorization) {
    const parts = req.headers.authorization.split(' ');
    if (parts.length === 2) {
      const scheme = parts[0];
      const credentials = parts[1];
      if (schemesSupported.test(scheme)) {
        return credentials;
      }
    }
  }

  return null;
}
function decodeUserKey(userKey) {
  const array = userKey.split(':');
  return {
    country: array[0],
    documentNumber: array[2],
    documentType: array[1]
  };
}
function loadCertJWT(path) {
  if (!fs.existsSync(path)) {
    logger.error('', '[validate-jwt-middleware][loadCertJWT] Missing cert JWT file.');
    const error = new Error('Missing cert JWT file.');
    throw error;
  }
  return fs.readFileSync(path);
}
const cert = loadCertJWT(config.certJwt);
module.exports.validateJwtMiddleware = (req, res, next) => {
  const token = getToken(req);
  if (!token) {
    logger.error('', 'the check req code qr is not correct');
    throw new UnauthorizedError('ERROR_NO_AUTHORIZATION', 'No authorization token was found');
  }
  const dtoken = jwt.decode(token, { complete: true }) || {};
  // CHECK IF THE TOKEN HAVE THE BEARER TYPE!
  if (!dtoken.payload) {
    throw new UnauthorizedError('ERROR_NO_AUTHORIZATION', 'The authorization token is invalid');
  }
  // verify a token asymmetric

  jwt.verify(token, cert, (err, decoded) => {
    if (err) throw new UnauthorizedError('ERROR_NO_AUTHORIZATION', err.message);
    // eslint-disable-next-line prefer-const
    let user = decoded;
    /* istanbul ignore else */
    if (user.user_key) {
      user.user_key = decodeUserKey(decoded.user_key);
    }
    req.user = user;
    return next();
  });
};
