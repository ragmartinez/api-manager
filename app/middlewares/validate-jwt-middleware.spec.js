const path = require('path');
const mockery = require('mockery');
const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;

describe('validateJwtMiddleware', () => {

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
  });

  afterEach(function () {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('validateJwtMiddleware middleware', () => {

    it('should return next() when the jwt token is present and create user in the request', async function () {
      let request = {};
      request.headers = getHeader().VALID_WITHOUT_USER_KEY;
      const nextSpy = sinon.spy();
      mockery.registerMock('../config', getConfing().VALID_CERT_JWT);
      let logger = {
        info: () => {},
        debug: () => {},
        warn: () => {},
        error: () => {},
        fatal: () => {}
      }
      let mockLogger = {
        Logger: { getLogger: () => logger }
      }
      mockery.registerMock('fif-common-node-logger', mockLogger);

      let middleware = require("./validate-jwt-middleware").validateJwtMiddleware;
      await middleware(request, null, nextSpy);

      expect(request.user).to.be.exist;
      expect(request.user.type).to.be.equal('ApiKey');
      expect(nextSpy.called).to.be.true;
    });

    it('should throw error when the jwt token is not present', async function () {
      let request = {};
      let logger = {
        info: () => {},
        debug: () => {},
        warn: () => {},
        error: () => {},
        fatal: () => {}
      }
      let mockLogger = {
        Logger: { getLogger: () => logger }
      }
      mockery.registerMock('fif-common-node-logger', mockLogger);
      let middleware = require("./validate-jwt-middleware").validateJwtMiddleware;

      try {
        await middleware(request, null, null);
      } catch (e) {
        expect(e.status).to.be.equal(401);
        expect(e.code).to.be.equal("ERROR_NO_AUTHORIZATION");
      }
    });

    it('should throw error when the jwt token is not valid', async function () {
      let request = {};
      request.headers = getHeader().NOT_VALID;
      let logger = {
        info: () => {},
        debug: () => {},
        warn: () => {},
        error: () => {},
        fatal: () => {}
      }
      let mockLogger = {
        Logger: { getLogger: () => logger }
      }
      mockery.registerMock('fif-common-node-logger', mockLogger);
      let middleware = require("./validate-jwt-middleware").validateJwtMiddleware;

      try {
        await middleware(request, null, null);
      } catch (e) {
        expect(e.status).to.be.equal(401);
        expect(e.code).to.be.equal("ERROR_NO_AUTHORIZATION");
      }
    });

    it('should throw error when the jwt token not have payload type', async function () {
      let request = {};
      request.headers = getHeader().NOT_VALID_TYPE;
      let logger = {
        info: () => {},
        debug: () => {},
        warn: () => {},
        error: () => {},
        fatal: () => {}
      }
      let mockLogger = {
        Logger: { getLogger: () => logger }
      }
      mockery.registerMock('fif-common-node-logger', mockLogger);
      mockery.registerMock('../config', {
        config: getConfing().VALID_CERT_JWT.config,
      });
     
      let middleware = require("./validate-jwt-middleware").validateJwtMiddleware;

      try {
        await middleware(request, null, null);
      } catch (e) {
        expect(e.status).to.be.equal(401);
        expect(e.code).to.be.equal("ERROR_NO_AUTHORIZATION");
      }
    });

    it('should throw error when the jwt token is not authorized', async function () {
      let request = {};
      request.headers = getHeader().NOT_AUTHORIZED;
      mockery.registerMock('../config', getConfing().VALID_CERT_JWT);
      let logger = {
        info: () => {},
        debug: () => {},
        warn: () => {},
        error: () => {},
        fatal: () => {}
      }
      let mockLogger = {
        Logger: { getLogger: () => logger }
      }
      mockery.registerMock('fif-common-node-logger', mockLogger);
      let middleware = require("./validate-jwt-middleware").validateJwtMiddleware;
      try {
        await middleware(request, null, null);
      } catch (e) {
        expect(e.status).to.be.equal(401);
        expect(e.code).to.be.equal("ERROR_NO_AUTHORIZATION");
      }
    });

    it('should throw error when loadCertJWT method return error Missing cert JWT file.', async function () {
      let logger = {
        info: () => {},
        debug: () => {},
        warn: () => {},
        error: () => {},
        fatal: () => {}
      }
      let mockLogger = {
        Logger: { getLogger: () => logger }
      }
      mockery.registerMock('fif-common-node-logger', mockLogger);
      try {
        let middleware = require("./validate-jwt-middleware").validateJwtMiddleware;
        await middleware({}, null, null);
      } catch (e) {
        expect(e.message).to.be.equal("No authorization token was found");
      }
    });
  });
});

function getHeader() {
  return {
    VALID_WITHOUT_USER_KEY: {
      'Content-Type': 'application/json',
      authorization: "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmltYXJ5c2lkIjoiNWQzNWJkMGY3ZjY1NjcwMDFkNGUwNDEzIiwidW5pcXVlX25hbWUiOiJXYWxsZXQgQkQgUUEiLCJncm91cHNpZCI6IkFQUEwiLCJpc3MiOiJTaW5nbGVTaWduT24iLCJhdWQiOiJTZXJ2ZXIiLCJ0eXBlIjoiQXBpS2V5IiwidXNlcl9rZXkiOiJDTDpSVVQ6MTc3Njk0ODg2Iiwic2NvcGVzIjpbInByb2ZpbGUiXSwiaWF0IjoxNTYzODg4MzIwLCJleHAiOjE2MjcwMDM1MjB9.SnkZTOgBd4ZlLf9PR_uDZlnFqMiMdP5UbK9AIsN0xHy8lhXn8O6JzGhJ48RVVq18pPyXGYF6pZyCxhr7WR7-vZ7zlwBnLI7rpjp912d382SmV8chw95h2Nz3H85ipY0qoxIEDsekpokrArDjTbBMVR3lubt9XaqcWZt4x6ZXWCaJP3fhtW769EiXvxvo_1AreoD4tQyYVA4nKjf9KBkgJvawXeG1m3mvg4-CdmYKR82MIAVUzV0V5jIWZqWINaQpvCq_GnbGroSwO1xAzoDPTWujPDpSKDXC6uODqeTs6DU0Wqwebgk8EZY_WzIY9C174hNZsGaK7aY8S1CE3PDDfQ"
    },
    NOT_VALID: {
      'Content-Type': 'application/json',
      authorization: "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmltYXJ5c2lkIjoiNWQzNWJkMGY3ZjY1NjcwMDFkNGUwNDEzIiwidW5pcX"
    },
    NOT_VALID_TYPE: {
      'Content-Type': 'application/json',
      authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
    },
    NOT_AUTHORIZED: {
      'Content-Type': 'application/json',
      authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmltYXJ5c2lkIjoiNWQzNWJkMGY3ZjY1NjcwMDFkNGUwNDEzIiwidW5pcXVlX25hbWUiOiJXYWxsZXQgQkQgUUEiLCJncm91cHNpZCI6IkFQUEwiLCJpc3MiOiJTaW5nbGVTaWduT24iLCJhdWQiOiJTZXJ2ZXIiLCJ0eXBlIjoiQXBpS2V5Iiwic2NvcGVzIjpbInByb2ZpbGUiXSwiaWF0IjoxNTYzODg4MzIwLCJleHAiOjE2MjcwMDM1MjB9.SHxYO9OO_PJNZvR68rHOZ9aAiLBERtjm9fMSzHzdAA0"
    },
  }
}

function getConfing() {
  const certJwt = path.resolve(__dirname, '../config/certs/api-manager-jwt-test.crt');
  return {
    VALID_CERT_JWT: {
      config: { certJwt }
    }
  }
}
