const GetPointsController = require('../controllers/get-points-controller');
const logger = require('fif-common-node-logger').Logger.getLogger();
const setResponseRaw = require('../util/common-response').setResponseRaw;
const _ = require('lodash');
const BadRequestError = require('../util/common-errors').BadRequestError;


const moduleName = 'getPointsMiddleware';

module.exports.getPointsMiddleware = async (req, res, next) => {
  const method = 'getPointsMiddleware';
  const accountId = _.get(req, 'params.accountId');
  if (!accountId) {
    throw new BadRequestError('ERROR_ACCOUNT_ID_NOT_VALID', 'the Account Id is not correct');
  }
  logger.info('', `[${moduleName}][${method}] init get-points-middleware`);
  try {
    const controller = new GetPointsController();
    res.response = await controller.getPoints(accountId);
    return setResponseRaw(res, 200, res.response);
  } catch (e) {
    logger.error('', `[${moduleName}][${method}] error: ${JSON.stringify(e)}`);
    return next(e);
  }
};
