const mockery = require('mockery');
const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;

describe('getPointsMiddleware', () => {
  beforeEach(function () {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    let logger = {
      info: () => {},
      debug: () => {},
      warn: () => {},
      error: () => {},
      fatal: () => {}
    }
    let mockLogger = {
      Logger: { getLogger: () => logger }
    }
    mockery.registerMock('fif-common-node-logger', mockLogger);
  });

  afterEach(function () {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('getPointsMiddleware middleware', () => {
    it('should return the response correctly', async function () {
      let request = {};
      request.headers = {};
      request.params = {};
      request.headers['Authorization'] = 'clave';
      request.params['accountId'] = '123456';
      request.headers['x-flow-country'] = 'cl';
      const nextSpy = sinon.spy();

      let mockResponse = {
        setResponseRaw: () => {
        }
      };

      const responseSpy = sinon.spy(mockResponse, 'setResponseRaw');
      mockery.registerMock('../util/common-response', mockResponse);
      mockery.registerMock('../controllers/get-points-controller', function () {
        this.getInstallments = () => {
          return true
        }
      });
      let middleware = require('./get-points-middleware').getPointsMiddleware;
      await middleware(request, {}, nextSpy);

      expect(nextSpy.called).to.be.true;
    });

   

    it('should throw error when get points fails', async function () {
      let request = {};
      request.headers = {};
      request.params = {};
      request.headers['Authorization'] = 'clave';
      request.params['accountId'] = '123456';
      request.headers['x-flow-country'] = 'cl';

      const nextSpy = sinon.spy();
      mockery.registerMock('../controllers/get-points-controller', function () {
        this.getPoints = () => {
          throw new Error('Error on getPoints');
        }
      });
      let middleware = require('./get-points-middleware').getPointsMiddleware;
      await middleware(request, null, nextSpy);
      console.log(nextSpy);
      expect(nextSpy.called).to.be.true;
      expect(nextSpy.args[0][0].message).to.be.equal('Error on getPoints');
    });
  });
});
