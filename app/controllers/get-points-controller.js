const logger = require('fif-common-node-logger').Logger.getLogger();
const apiCmrpuntosService = require('../services/api-cmrpuntos/api-cmrpuntos-service');

const moduleName = 'GetPointsController';

module.exports = class GetPointsController {
  async getPoints(accountId) {
    const method = 'getPoints';
    logger.info(accountId, `[${moduleName}][${method}] Try get points.`);
    try {
      const response = await apiCmrpuntosService.getPoints(accountId);
      logger.info(accountId, `[${moduleName}][${method}] response api cmr puntos ${JSON.stringify(response)}`);
      return response;
    } catch (error) {
      throw error;
    }
  }
};
