const mockery = require('mockery');
const sinon = require('sinon');
const sandbox = sinon.createSandbox();
const chai = require('chai');
const expect = chai.expect;

let controller;
let apiCmrPuntosService;


describe('GetPointsController', () => {

  beforeEach(function () {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    let logger = {
      info: () => {},
      debug: () => {},
      warn: () => {},
      error: () => {},
      fatal: () => {}
    }
    let mockLogger = {
      Logger: { getLogger: () => logger }
    }
    mockery.registerMock('fif-common-node-logger', mockLogger);

    if (!controller && !apiCmrPuntosService) {
      const GetPointsController = require('./get-points-controller');
      controller = new GetPointsController();
      apiCmrPuntosService = require('../services/api-cmrpuntos/api-cmrpuntos-service');

    }
  });



  afterEach(function () {
    sandbox.restore();
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('execute method', () => {

    
    it('should return succes api-cmrpuntos service response', async function () {

      let request = {};
      request.headers = {};
      request.params = {};
      request.headers['Authorization'] = 'clave';
      request.params['accountId'] = '123456';
      request.headers['x-flow-country'] = 'cl';

      sandbox
        .stub(apiCmrPuntosService, 'getPoints')
        .resolves({
          "code": 200,
          "status": "SUCCESS",
          "points": {
              "aboutToExpire": 0,
              "closestExpirationDate": "01/04/2021",
              "earned": 0,
              "balance": 2335986
          }
      });

      const response = await controller.getPoints(request);
      expect(response);
    });

   it('should return error ', async function () {

      let request = {};
      request.headers = {};
      request.params = {};
      request.headers['Authorization'] = 'clave';
      request.headers['x-flow-country'] = 'cl';

      sandbox
        .stub(apiCmrPuntosService, 'getPoints')
        .resolves({
          "code": 500,
          "status": "ERROR"
      });

      try {
        await controller.getInstallments(req);
      } catch (e) {
        expect(e.status).to.be.equal(500);
        expect(e.code).to.be.equal("ERROR_FLOW_NOT_SUPPORTED");
        expect(e.message).to.be.equal("flow temporarily unsupported");
      }
    });

  }); 
});

