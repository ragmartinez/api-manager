const mockery = require('mockery');
const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;

describe('Get installments Route', () => {
  beforeEach(function () {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

  });

  afterEach(function () {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('checking the actions', () => {
    it('should allow calls by using GET ', function () {
      const actionsMock = {
        get: function (path, middlewares) {
        }
      };
      const getSpy = sinon.spy(actionsMock, 'get');
      let expressMock = {
        Router: function () {
          return actionsMock;
        }
      };
      mockery.registerMock('express', expressMock);
      mockery.registerMock('../config', {
        context: {
          middlewares: {
            getInstallments: ["middleware"]
          }
        }
      });

      const getMiddlewaresMock = {
        getMiddlewares: function (middlewares) {
          return middlewares;
        }
      };

      mockery.registerMock('../util/get-middleware', getMiddlewaresMock);
      require("./get-points-route");

      expect(getSpy.called).to.be.true;
      expect(getSpy.args[0][0]).to.be.equal("/get-points/:accountId/points");

    });
  });
});
