const Router = require('express').Router;
const context = require('../config').context;
const getMiddlewares = require('../util/get-middleware').getMiddlewares;

const router = Router();

router.get('/get-points/:accountId/points', getMiddlewares(context.middlewares.getPoints));
module.exports = router;
