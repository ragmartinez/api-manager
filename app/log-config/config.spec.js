const mockery = require('mockery');
const chai = require('chai');
const expect = chai.expect;

describe('config', () => {
  beforeEach(function () {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

  });

  afterEach(function () {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('config', () => {
    it('should return logger config', function () {
      mockery.registerMock('fif-payments-common-logger', {
        setConfig: () => {
        }
    });

      require("./config");
    });
  });
});
