const logConfig = require('fif-common-node-logger').setConfig.trace;

const config = {
  env: process.env.NODE_ENV,
  envName: 'NODE_ENV',
  envDefault: 'local',
  level: process.env.NODE_LOG_LEVEL || 'info',
  filePath: process.env.NODE_LOG_FILE_PATH || '',
  fileNameFormat: process.env.NODE_LOG_FILE_NAME_FORMAT || 'generic',
  fileNameType: process.env.NODE_LOG_FILE_NAME_TYPE || 'service-date-hostname',
  toStdout: process.env.NODE_LOG_TO_STDOUT || 'true',
  toFile: process.env.NODE_LOG_TO_FILE || 'true',
  enabled: process.env.NODE_LOG_ENABLED || 'true',
  serviceName: process.env.NODE_LOG_SERVICE_NAME || 'fif-payments-api-manager-points',
  outputFormat: process.env.NODE_LOG_OUTPUT_FORMAT || 'pino-log-generic',
  inputFormat: process.env.NODE_LOG_INPUT_FORMAT || 'module-method-description-message',
  internalInterceptors: process.env.NODE_LOG_INTERNAL_INTERCEPTORS || [],
  externalInterceptors: process.env.NODE_LOG_EXTERNAL_INTERCEPTORS || [],
  extraGlobalInfo: { environment: process.env.NODE_ENV, country: process.env.NODE_COUNTRY },
  setDate: process.env.NODE_LOG_SET_DATE || { name: '@timestamp', format: 'default' },
  nameSpaceHook: process.env.NODE_LOG_NAMESPACE_HOOK || 'app.my-app',
  traceHeaders: process.env.NODE_LOG_TRACE_HEADERS || [{ header: 'kong-request-id', name: 'trace-id' }],
  traceNewValues: process.env.NODE_LOG_TRACE_NEW_VALUES || null,
  isExpress: process.env.NODE_LOG_IS_EXPRESS || 'true',
  timeTocheckSize: process.env.NODE_LOG_TTL_CHECK_SIZE || 10000,
  sizeToRotate: process.env.NODE_LOG_SIZE_TO_ROTATE || 1,
  enableCleanFile: process.env.NODE_LOG_ENABLE_CLEAN_FILE || 'true'
};
module.exports.init = logConfig.setConfig(config);
module.exports.default = config;
